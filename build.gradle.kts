plugins {
    java
	application
	
}

group = "it.lundstedt.erik"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
	flatDir {
		dirs("libs")
	}
/*	flatDir {
		dirs("lib1", "lib2")
	}
*/
}
dependencies {
    //testCompile("junit", "junit", "4.12")
	runtimeOnly(fileTree("libs") { include("*.jar") })
	//runtimeOnly(files("lib/openMenuLib.jar"))
	default(files("libs/openMenuLib.jar"))
	compileOnly(files("libs/openMenuLib.jar"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

application {
	mainClassName = "Main"
}
tasks.register("hello") {
	doFirst{
		println("first")
	}
	doLast {
		println("last")
	}
}
fun jarAndRun ()
{
//	println("jar and run")
}

tasks.register("jarAndRun")
{
	dependsOn ("jar","run")
	jarAndRun()
}

tasks.run {


}

tasks.jar {
	manifest {
		attributes(
				"Main-Class" to "Main"
		)
	}
}



